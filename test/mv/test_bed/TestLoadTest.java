/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.test_bed;

import javafx.collections.ObservableList;
import mv.data.DataManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static junit.framework.TestCase.assertEquals;
import mv.data.Subregion;

/**
 *
 * @author yushanzhou
 */
public class TestLoadTest {
    
    public TestLoadTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of main method, of class TestLoad.
     */
    @Test
    public void testMain() {
//        System.out.println("main");
//        String[] args = null;
//        TestLoad.main(args);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of loadAndorraMap method, of class TestLoad.
     */
    @Test
    public void testLoadAndorraMap() {
        System.out.println("loadAndorraMap");
        DataManager dm1 = TestSave.createAndorraMap();
        DataManager dm2 = TestLoad.loadAndorraMap();
        
        String mapname1 = dm1.getMapName();
        String mapname2 = dm2.getMapName();
        assertEquals(mapname1, mapname2);
        
        double c1 = dm1.getBgColor().getBlue();
        double c2 = dm2.getBgColor().getBlue();
        assertEquals(c1, c2);
        
        double w1 = dm1.getBorderWidth();
        double w2 = dm2.getBorderWidth();
        assertEquals(w1, w2);

        String sub1 = dm1.getSubregion().get(0).getSubregionName();
        String sub2 = dm2.getSubregion().get(0).getSubregionName();
        assertEquals(sub1, sub2);
        
        String path1 = dm1.getImage1Path();
        String path2 = dm2.getImage1Path();
        assertEquals(path1, path2);

        
//        assertEquals
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }

    /**
     * Test of loadSanMarinoMap method, of class TestLoad.
     */
    @Test
    public void testLoadSanMarinoMap() {
        System.out.println("loadSanMarinoMap");
        DataManager dm1 = TestSave.createSanMarinoMap();
        DataManager dm2 = TestLoad.loadSanMarinoMap();
        String mapname1 = dm1.getMapName();
        String mapname2 = dm2.getMapName();
        assertEquals(mapname1, mapname2);
        
        double c1 = dm1.getBgColor().getBlue();
        double c2 = dm2.getBgColor().getBlue();
        assertEquals(c1, c2);
        
        double w1 = dm1.getBorderWidth();
        double w2 = dm2.getBorderWidth();
        assertEquals(w1, w2);

        String sub1 = dm1.getSubregion().get(0).getFlagPath();
        String sub2 = dm2.getSubregion().get(0).getFlagPath();
        assertEquals(sub1, sub2);
        
        String path1 = dm1.getImage1Path();
        String path2 = dm2.getImage1Path();
        assertEquals(path1, path2);


    }

    /**
     * Test of loadSlovakiaMap method, of class TestLoad.
     */
    @Test
    public void testLoadSlovakiaMap() {
        System.out.println("loadSlovakiaMap");
        DataManager dm1 = TestSave.createSlovakiaMap();
        DataManager dm2 = TestLoad.loadSlovakiaMap();
        String mapname1 = dm1.getMapName();
        String mapname2 = dm2.getMapName();
        assertEquals(mapname1, mapname2);
        
        double c1 = dm1.getBgColor().getBlue();
        double c2 = dm2.getBgColor().getBlue();
        assertEquals(c1, c2);
        
        double w1 = dm1.getBorderWidth();
        double w2 = dm2.getBorderWidth();
        assertEquals(w1, w2);

        String sub1 = dm1.getSubregion().get(0).getSubregionName();
        String sub2 = dm2.getSubregion().get(0).getSubregionName();
        assertEquals(sub1, sub2);
        
        String path1 = dm1.getImage1Path();
        String path2 = dm2.getImage1Path();
        assertEquals(path1, path2);      
    }
    
}
