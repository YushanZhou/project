/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.shape.Polygon;
import mv.gui.Workspace;
/**
 *
 * @author Yushan Zhou
 */
public class PolygonData {
    
    List<Polygon> polygons;
    
    public PolygonData(){
        polygons = new ArrayList<>();
        
    }
    
    public List<Polygon> getPolygons(){
        return polygons;
    }
    
    public void add(Polygon p){
        polygons.add(p);
    }
    
}
