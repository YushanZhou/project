package mv.data;

import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import saf.components.AppDataComponent;
import mv.RegioVincoMapEditor;
import mv.gui.Workspace;

/**
 *
 * @author McKillaGorilla
 */
public class DataManager implements AppDataComponent {
    
    List<PolygonData> polygonList;
    
    
    RegioVincoMapEditor app;
    String mapName;
    
    String mapPath;
    
    String mapFolder;
    
    Color bgColor;
    
    String image1Path;
    
    int image1PosX;
    
    int image1PosY;
    
    int image2PosX;
    
    int image2PosY;
    
    String image2Path;
    
    Color borderColor;
    
    double borderWidth;
    
    int Scale;
    
    int mapPosX;
    
    int mapPosY;
    
    int mapDimensionX;
    
    int mapDimensionY;
    
    String anthempath;
    
    ObservableList<Subregion> data;
    
    public DataManager(RegioVincoMapEditor initApp) {
        app = initApp;
        polygonList = new ArrayList<>();
        data = FXCollections.observableArrayList();
    }
    
    public DataManager(){
        polygonList = new ArrayList<>();
        data = FXCollections.observableArrayList();
    }
    
    @Override
    public void reset() {
        mapName = "";
        mapPath = "";
        mapFolder = "";
        image1Path = "";
        image2Path = "";
        anthempath = "";
        polygonList.clear();
        data.clear();
        ((Workspace)app.getWorkspaceComponent()).removeRoot();
    }
    
    
    public void setMapName(String name){
        mapName = name;
    }
    
    public String getMapName(){
        return mapName;
    }
    
    public void setMapPath(String path){
        mapPath = new String(path);
    }
    
    public String getMapPath(){
        return mapPath;
    }
    
    public void setMapFolder(String folder){
        mapFolder = new String(folder);
    }
    
    public String getMapFolder(){
        return mapFolder;
    }
    
    public void setBgColor(int red, int green, int blue){
        bgColor = Color.rgb(red, green, blue);
    }
    
    public void setBgColorFromPicker(Color c){
        bgColor = c;
    }
    
    public Color getBgColor(){
        return bgColor;
    }
     
    
    public void setImage1(String image, int x, int y){
        image1Path = image;
        image1PosX = x;
        image1PosY = y;
    }
    
    public String getImage1Path(){
        return image1Path;
    }
    
    public int getImage1PosX(){
        return image1PosX;
    }
    
    public int getImage1PosY(){
        return image1PosY;
    }
    
    public void setImage2(String image, int x, int y){
        image2Path = image;
        image2PosX = x;
        image2PosY = y;
        
    }
    
    public String getImage2Path(){
        return image2Path;
    }
    
    public int getImage2PosX(){
        return image2PosX;
    }
    
    public int getImage2PosY(){
        return image2PosY;
    }
    
    public void setBorder(int red, int green, int blue, double width){
        borderColor = Color.rgb(red, green, blue);
        borderWidth = width;
        
    }
    
    public void setBorderColorFromPicker(Color c){
        borderColor = c;
    }
    
    public Color getBorderColor(){
        return borderColor;
    }
    
    public double getBorderWidth(){
        return borderWidth;
    }
    
    public void setMapSacle(int zoom){
        Scale = zoom;
    }
    
    public void setmapPos(int x, int y){
        mapPosX = x;
        mapPosY = y;
    }
    
    public int getMapScale(){
        return Scale;
    }
    
    public int getMapPosX(){
        return mapPosX;
    }
    
    public int getMapPosY(){
        return mapPosY;
    }
    
    public void setSubregion(ObservableList<Subregion> data){
        this.data = data;
    }
    
    public List<PolygonData> getPolygonList() {
        return polygonList;
    }
    
    public ObservableList<Subregion> getSubregion(){
        return data;
    }

    public void setBorderWidth(double thicknessValue) {
                borderWidth = thicknessValue;

     }
    
    public void setMapDimension(int x, int y){
        mapDimensionX = x;
        mapDimensionY = y;
    }
    
    public void setAnthemPath(String path){
        anthempath = path;
    }
    
    public String getAnthemPath(){
        return anthempath;
    }
    
    public void updateWorkspace(){
        ((Workspace)app.getWorkspaceComponent()).reloadWorkspace();
        ((Workspace)app.getWorkspaceComponent()).loadImage();
    }

    public void setImage1Pos(int x, int y) {
        image1PosX = x;
        image1PosY = y;
    }

    public void setImage2Pos(int x, int y) {
        image2PosX = x;
        image2PosY = y;
    
    }
    
    public void setMapDimensionX(int x){
        mapDimensionX = x;
    }
    
    public void setMapDimensionY(int y){
        mapDimensionY = y;
    }
    
    public int getMapDinmensionX(){
        return mapDimensionX;
    }
    
    public int getMapDimensionY(){
        return mapDimensionY;
    }
    
}
