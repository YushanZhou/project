/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.data;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author yushanzhou
 */
public class Subregion {
    
    private  SimpleStringProperty subregionName;
    private  SimpleStringProperty capital;
    private  SimpleStringProperty leader;
    private Color color;
    private String leaderPhotoPath;
    private String flagPath;
    
    public Subregion(){
        
    }
 
    public Subregion(String subregion, String capital, String leader, int x, int y, int z,
            String flag, String leaderPhoto) {
        this.subregionName = new SimpleStringProperty(subregion);
        this.capital = new SimpleStringProperty(capital);
        this.leader = new SimpleStringProperty(leader);
        setColor(x,y,z);
        setLeaderPhoto(leaderPhoto);
        setFlagPath(flag);
        
    }
 
    public String getSubregionName() {
        return subregionName.get();
    }
    public void setSubregionName(String Name) {
        subregionName.set(Name);
    }
        
    public String getCapital() {
        return capital.get();
    }
    public void setCapital(String fName) {
        capital.set(fName);
    }
    
    public String getLeader() {
        return leader.get();
    }
    public void setLeader(String fName) {
        leader.set(fName);
    }
    
    public void setColor(int x, int y, int z){
        color = Color.rgb(x, y, z);
        
    }
    
    public Color getColor(){
        return color;
    }
    
    public void setLeaderPhoto(String path){
        leaderPhotoPath = path;
    }
    
    public String getLeaderPhoto(){
        return leaderPhotoPath;
    }
    
    public void setFlagPath(String path){
        flagPath = path;
    }
    
    public String getFlagPath(){
        return flagPath;
    }
    
    
}
