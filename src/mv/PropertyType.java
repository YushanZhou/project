/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv;

/**
 *
 * @author McKillaGorilla
 */
public enum PropertyType {
    OK_PROMPT,
    CANCEL_PROMPT,
    SUBREGION_COLUMN_HEADING,
    CAPITAL_COLUMN_HEADING,
    LEADER_COLUMN_HEADING,
    
    //toolbar button icons
    MAP_NAME_ICON,
    ADD_IMAGE_ICON,
    REMOVE_IMAGE_ICON,
    BG_COLOR_ICON,
    BORDER_COLOR_ICON,
    BORDER_THICKNESS_ICON,
    MAP_COLOR_ICON,
    ZOOM_ICON,
    MAP_DIMENSION_ICON,
    PLAY_ANTHEM_ICON,
    PAUSE_ICON,
    PAUSE_TOOLTIP,
    
    //toolbar button tooltips
    MAP_NAME_TOOLTIP,
    ADD_IMAGE_TOOLTIP,
    REMOVE_IMAGE_TOOLTIP,
    BG_COLOR_TOOLTIP,
    BORDER_COLOR_TOOLTIP,
    BORDER_THICKNESS_TOOLTIP,
    MAP_COLOR_TOOLTIP,
    ZOOM_TOOLTIP,
    MAP_DIMENSION_TOOLTIP,
    PLAY_ANTHEM_TOOLTIP,
    
    
}
