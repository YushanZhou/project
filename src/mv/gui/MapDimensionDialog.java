/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author yushanzhou
 * 
 */
public class MapDimensionDialog extends Stage{
    
    
    static MapDimensionDialog singleton = null;
    //private AppItemDialog itemDialog = null;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane messagePane;
    Scene messageScene;
    Button okButton;
    Button cancelButton;
    String selection;
    TextField mapWidth;
    TextField mapHeight;
    int w;
    int h;
    
    Workspace workspace;
    
    Label width;
    Label height;
    // CONSTANT CHOICES

    /**
     *
     */
    public static final String OK = "Ok";

    /**
     *
     */
    public static final String CANCEL = "Cancel";
    
    
    /**
     * private constructor
     */
    private MapDimensionDialog() {}
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static MapDimensionDialog getSingleton() {
        if(singleton == null)
            singleton = new MapDimensionDialog();
	return singleton;
    }
    
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
              
        // SAVE AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);
        

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancelButton);
         
        mapWidth = new TextField();
        mapHeight = new TextField();
        
        width = new Label("Map Width");
        height = new Label("Map Height");
        
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new GridPane();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.setHgap(10);
        messagePane.setVgap(10);
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        
        messagePane.add(width, 0, 0);
        messagePane.add(mapWidth, 1, 0);
        messagePane.add(height, 0, 1);
        messagePane.add(mapHeight, 1, 1);
        messagePane.add(buttonBox, 1, 5);
        
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
        
        
        
        
	
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler SaveHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            w = Integer.parseInt(mapWidth.getText());
            h = Integer.parseInt(mapHeight.getText());
            workspace.ChangeDimension(w, h);
            MapDimensionDialog.this.hide();
        };
        
        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler CancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            MapDimensionDialog.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(SaveHandler);
        cancelButton.setOnAction(CancelHandler);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public void setMapWidth(int w){
        mapWidth.setText(Integer.toString((int) workspace.getR1().getWidth()));
    }
    
    public int getMapWidth(){
        return w;
    }
    
    public void setMapHeight(int h){
        mapHeight.setText(Integer.toString((int) workspace.getR1().getHeight()));
    }
    
    public int getMapHeight(){
        return h;
    }
    
    public void setWorkspace(Workspace w){
        workspace = w;
    }
 
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        show();
    }
    
   

}
