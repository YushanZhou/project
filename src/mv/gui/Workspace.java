/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;


import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.*;
import javafx.scene.shape.Polygon;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Transform;
import javax.imageio.ImageIO;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.sampled.Clip;
import mv.PropertyType;
import static mv.PropertyType.*;
import saf.components.AppWorkspaceComponent;
import mv.RegioVincoMapEditor;
import mv.controller.MapEditorController;
import mv.data.DataManager;
import mv.data.Subregion;
import mv.data.PolygonData;
import properties_manager.PropertiesManager;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import saf.ui.AppGUI;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author McKillaGorilla
 * @author Yushan Zhou
 */
public class Workspace extends AppWorkspaceComponent {
    RegioVincoMapEditor app;
    
    AppGUI gui;
    
    DataManager dataManager;
    
    MapEditorController controller;
    
    SplitPane splitpane;
    
    Group root;
    
    Pane imagepane;
    
    VBox table;
    
    GridPane toolbar;
    
    Button mapName;
    
    Button addImage;
    
    Button removeImage;
    
    ColorPicker bgColor;
    
    ColorPicker borderColor;
    
    Color subregionColor;
    
    Slider borderThickness;
    Label thicknessLabel;
    Label thicknessValue;
    
    Button mapColor;
    
    Slider zoom;
    Label zoomLabel;
    Label zoomValue;
    
    
    Button dimension;
    
    Button anthem;
    
    Button pause;
    
    TableView<Subregion> itemsTable;
    
    TableColumn subregionName;
    
    TableColumn capital;
    
    TableColumn leader;
    
    static double sceneWidth;
    static double sceneHeight;
    
    Rectangle r1;
    Rectangle r2;
    
    
    int previous = -2;
    int selectedimage;
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    ImageView iv1;
    ImageView iv2;
    
    //FOR KEEP TRACKING LAST INDEX
    boolean flag;
    int lastSelectedIndex;
    
    Sequencer sequencer;
    
    public Workspace(RegioVincoMapEditor initApp) {
        app = initApp;
        
        gui = app.getGUI();

        // INIT ALL WORKSPACE COMPONENTS
	layoutGUI();
        setupHandlers();
    }
    
    private void layoutGUI() {
        
        workspace = new Pane();
        
        dataManager = (DataManager)app.getDataComponent();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        splitpane = new SplitPane();
        
        imagepane = new Pane();
        
        r1 = new Rectangle();
        r2 = new Rectangle();
        imagepane.getChildren().add(r1);
        imagepane.setClip(r2);
        if(dataManager.getMapDinmensionX() == 0)
            r1.setWidth(802);
        else
            r1.setWidth(dataManager.getMapDinmensionX());
        if(dataManager.getMapDimensionY() == 0)
            r1.setHeight(536);
        else
            r1.setHeight(dataManager.getMapDimensionY());
        r1.setStroke(Color.BLACK);
        
        r2.layoutXProperty().bind(r1.layoutXProperty());
        r2.layoutYProperty().bind(r1.layoutYProperty());
        r2.widthProperty().bind(r1.widthProperty());
        r2.heightProperty().bind(r1.heightProperty());
        
        
        
        
        table = new VBox();
        
        toolbar = new GridPane();
        
        
        mapName = initChildButton(MAP_NAME_ICON.toString(),	    MAP_NAME_TOOLTIP.toString(),	false);
        
        addImage = initChildButton(ADD_IMAGE_ICON.toString(),	    ADD_IMAGE_TOOLTIP.toString(),	false);
    
        removeImage = initChildButton(REMOVE_IMAGE_ICON.toString(),	    REMOVE_IMAGE_TOOLTIP.toString(),	true);
    
        mapColor = initChildButton(MAP_COLOR_ICON.toString(),	   MAP_COLOR_TOOLTIP.toString(),	false);
         
        
        dimension = initChildButton(MAP_DIMENSION_ICON.toString(),	    MAP_DIMENSION_TOOLTIP.toString(),	false);
        
        anthem = initChildButton(PLAY_ANTHEM_ICON.toString(),	   PLAY_ANTHEM_TOOLTIP.toString(),	false);
        
        
        pause = initChildButton(PAUSE_ICON.toString(),	  PAUSE_TOOLTIP.toString(),	true);
        
        bgColor = new ColorPicker();
        bgColor.setTooltip( new Tooltip(props.getProperty(PropertyType.BG_COLOR_TOOLTIP)));
    
        borderColor = new ColorPicker();
        borderColor.setTooltip( new Tooltip(props.getProperty(PropertyType.BORDER_COLOR_TOOLTIP)));
    
        borderThickness = new Slider(0,0.8,dataManager.getBorderWidth());
        thicknessLabel = new Label("Border Thickness");
        thicknessValue = new Label(Double.toString(borderThickness.getValue()));
        borderThickness.setTooltip( new Tooltip(props.getProperty(PropertyType.BORDER_THICKNESS_TOOLTIP)));
        borderThickness.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    thicknessValue.setText(String.format("%.4f", new_val));
                    dataManager.setBorderWidth(new_val.doubleValue());
                    for(double i = 0; i < dataManager.getPolygonList().size(); i++) {
                     PolygonData data = dataManager.getPolygonList().get((int)i);
                     for(int j = 0; j < data.getPolygons().size(); j++){
                        Polygon p = data.getPolygons().get(j);
                        p.setStrokeWidth(dataManager.getBorderWidth());
                    }
                }
            }
        });
    
    
        zoom = new Slider(1,3000,dataManager.getMapScale());
        zoomLabel = new Label("Zoom Level");
        zoomValue = new Label(Double.toString(zoom.getValue()));
        zoom.setTooltip( new Tooltip(props.getProperty(PropertyType.ZOOM_TOOLTIP)));
        zoom.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                Number old_val, Number new_val) {
                    zoomValue.setText(String.format("%.2f", new_val));
                    dataManager.setMapSacle(new_val.intValue());
                    root.setScaleX(zoom.getValue());
                    root.setScaleY(zoom.getValue());
            }
        });
    
        
        itemsTable = new TableView();
        
        subregionName = new TableColumn(props.getProperty(PropertyType.SUBREGION_COLUMN_HEADING));
        
        capital = new TableColumn(props.getProperty(PropertyType.CAPITAL_COLUMN_HEADING));
        
        leader = new TableColumn(props.getProperty(PropertyType.LEADER_COLUMN_HEADING));
        
        
        sceneWidth = app.getGUI().getPrimaryScene().getWidth();
        sceneHeight = app.getGUI().getPrimaryScene().getHeight();
        
        splitpane.setPrefSize(sceneWidth, sceneHeight);
        toolbar.add(mapName, 0, 0);
        toolbar.add(addImage, 1, 0);
        toolbar.add(removeImage, 2, 0);
        toolbar.add(mapColor, 3, 0);
        toolbar.add(dimension, 4, 0);
        toolbar.add(anthem, 5, 0);
        toolbar.add(pause, 6, 0);
        toolbar.add(borderColor, 7, 0);
        toolbar.add(bgColor, 8, 0);
        toolbar.add(thicknessLabel, 0, 1, 3, 1);
        toolbar.add(borderThickness, 3, 1, 6, 1);
        toolbar.add(thicknessValue, 9, 1);
        toolbar.add(zoomLabel, 0, 2, 3, 2);
        toolbar.add(zoom, 3, 2, 6, 2);
        toolbar.add(zoomValue, 9, 2);
        
        table.getChildren().add(toolbar);
   
        subregionName.prefWidthProperty().bind(table.widthProperty().multiply(0.33));
        capital.prefWidthProperty().bind(table.widthProperty().multiply(0.33));
        leader.prefWidthProperty().bind(table.widthProperty().multiply(0.33));
        itemsTable.getColumns().add(subregionName);
        itemsTable.getColumns().add(capital);
        itemsTable.getColumns().add(leader);
        table.getChildren().add(itemsTable);
        
        splitpane.getItems().addAll(imagepane, table);
        workspace.getChildren().add(splitpane);
        
//        itemsTable.setFocusTraversable(false);
//        borderThickness.setFocusTraversable(false);
//        zoom.setFocusTraversable(false);
       
    }
    
    public Button initChildButton( String icon, String tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
	
	// LOAD THE ICON FROM THE PROVIDED FILE
        String imagePath = FILE_PROTOCOL + PATH_IMAGES + props.getProperty(icon);
        Image buttonImage = new Image(imagePath);
	ImageView iv = new ImageView(buttonImage);
        iv.setFitHeight(20);
        iv.setFitWidth(20);
	// NOW MAKE THE BUTTON
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(iv);
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip));
        button.setTooltip(buttonTooltip);
	
	// AND RETURN THE COMPLETED BUTTON
        return button;
    }
    
    public void removeRoot(){
        imagepane.getChildren().clear();
    }
    
    public void reloadTable(){
        if(dataManager.getSubregion() != null){
            ObservableList<Subregion> data = dataManager.getSubregion();
            itemsTable.getItems().clear();
            for(Subregion s: data){
                itemsTable.getItems().add(s);
                
                
            }
        }
    }
    
    public void reloadMap(){
        for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
            if(dataManager.getSubregion().size() < 1){
                subregionColor = Color.WHITE;
            }
            else{
                int red = (int)(dataManager.getSubregion().get((int)i).getColor().getRed()*255);
                int green = (int)(dataManager.getSubregion().get((int)i).getColor().getGreen()*255);
                int blue = (int)(dataManager.getSubregion().get((int)i).getColor().getBlue()*255);
                subregionColor = Color.rgb(red, green, blue);
            }
            
            PolygonData data = dataManager.getPolygonList().get((int)i);
            for(int j = 0; j < data.getPolygons().size(); j++){
                Polygon p = data.getPolygons().get(j);
                p.setFill(subregionColor);
            }
        }
    }
    
    @Override
    public void reloadWorkspace() {
        
//        if(dataManager.getMapDinmensionX() == 0)
//            splitpane.setDividerPosition(0, 802);
//        else{
//            splitpane.setDividerPosition(0, dataManager.getMapDinmensionX());
//            splitpane.setPrefHeight(dataManager.getMapDimensionY());
//            table.setLayoutX(sceneWidth - dataManager.getMapDinmensionX());
//            System.out.println("dataManager.getMapDinmensionX() " + dataManager.getMapDinmensionX());
//            System.out.println(splitpane.getWidth());
//            System.out.println("divider " + splitpane.getDividerPositions()[0]);
//            System.out.println("table x " + table.getLayoutX());
//        }


        if(dataManager.getSubregion() != null){
            ObservableList<Subregion> data = dataManager.getSubregion();
            // AND LINK THE COLUMNS TO THE DATA
            subregionName.setCellValueFactory(new PropertyValueFactory<String, String>("subregionName"));
            capital.setCellValueFactory(new PropertyValueFactory<String, String>("capital"));
            leader.setCellValueFactory(new PropertyValueFactory<String, String>("leader"));
            itemsTable.getItems().clear();
            for(Subregion s: data){
                itemsTable.getItems().add(s);
                
            }
        }
        
//        draw polygons
        root = new Group();
        int bred = (int)(dataManager.getBorderColor().getRed()*255);
        int bgreen = (int)(dataManager.getBorderColor().getGreen()*255);
        int bblue = (int)(dataManager.getBorderColor().getBlue()*255);
        Color border = Color.rgb(bred, bgreen, bblue);
        
        for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
            if(dataManager.getSubregion().size() < 1){
                subregionColor = Color.WHITE;
            }
            else{
                int red = (int)(dataManager.getSubregion().get((int)i).getColor().getRed()*255);
                int green = (int)(dataManager.getSubregion().get((int)i).getColor().getGreen()*255);
                int blue = (int)(dataManager.getSubregion().get((int)i).getColor().getBlue()*255);
                subregionColor = Color.rgb(red, green, blue);
            }
            
            PolygonData data = dataManager.getPolygonList().get((int)i);
            for(int j = 0; j < data.getPolygons().size(); j++){
                Polygon p = data.getPolygons().get(j);
                p.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        
                        Polygon polygonClicked = (Polygon)e.getSource();
                        if (e.getClickCount() == 1) {
                            boolean found = false;
                            for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
                                PolygonData data = dataManager.getPolygonList().get((int)i);
                                for(int j = 0; j < data.getPolygons().size(); j++){
                                    if(data.getPolygons().get(j).equals(polygonClicked)){
                                        highlightPolygon(i);
                                        highlightTableRow(i);
                                        found = true;
                                        break;
                                    }
                                    
                                }
                                if(found)
                                    break;
                            }
                            
                        }
                        
                        if (e.getClickCount() == 2) {
                             boolean found = false;
                            for(int i = 0; i < dataManager.getPolygonList().size(); i++) {
                                PolygonData data = dataManager.getPolygonList().get((int)i);
                                for(int j = 0; j < data.getPolygons().size(); j++){
                                    if(data.getPolygons().get(j).equals(polygonClicked)){
                                        
                                        highlightPolygon(i);
                                        highlightTableRow(i);
                                        controller.processEditSubregion(i);
                                        found = true;
                                        break;
                                    }
                                    
                                }
                                if(found)
                                    break;
                            }
                        }
                    }
                });
                p.setFill(subregionColor);
                root.getChildren().add(p);
                p.setStrokeWidth(dataManager.getBorderWidth());
                p.setStroke(border);
                
            }
        }
        
        int bgred = (int)(dataManager.getBgColor().getRed()*255);
        int bggreen = (int)(dataManager.getBgColor().getGreen()*255);
        int bgblue = (int)(dataManager.getBgColor().getBlue()*255);
        Color bg = Color.rgb(bgred, bggreen, bgblue);

        imagepane.setBackground(new Background(new BackgroundFill(bg, null, null)));

        imagepane.getChildren().add(root);
        
        root.setScaleX(dataManager.getMapScale());
        root.setScaleY(dataManager.getMapScale());
        
        if(dataManager.getMapPosX() != 0 || dataManager.getMapPosY() != 0){
            root.setTranslateX(dataManager.getMapPosX());
            root.setTranslateY(dataManager.getMapPosY());
        }
    }
    
    public void loadImage(){
        if(!dataManager.getImage1Path().equals("")){
            ImageView iv1 = new ImageView(new Image(dataManager.getImage1Path()));
            addImage1(iv1);
            iv1.setTranslateX(dataManager.getImage1PosX());
            iv1.setTranslateY(dataManager.getImage1PosY());
            
        }
        
        if(!dataManager.getImage2Path().equals("")){
            ImageView iv2 = new ImageView(new Image(dataManager.getImage2Path()));
            addImage2(iv2);
            iv2.setTranslateX(dataManager.getImage2PosX());
            iv2.setTranslateY(dataManager.getImage2PosY());
        }
    }
    
    public void addImage1(ImageView i1){
        iv1 = i1;
        imagepane.getChildren().add(iv1);
        iv1.setOnMousePressed(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent t) {
                            if(selectedimage == 1){
                                getRemoveImageButton().setDisable(true);
                                iv1.setEffect(null);
                                selectedimage = 0;
                            }
                            else{
                                selectedimage = 1;
                                getRemoveImageButton().setDisable(false);
                                iv1.setEffect(new DropShadow(50, Color.BLUE));
                                orgSceneX = t.getSceneX();
                                orgSceneY = t.getSceneY();
                                orgTranslateX = ((ImageView)(t.getSource())).getTranslateX();
                                orgTranslateY = ((ImageView)(t.getSource())).getTranslateY();
                            }
                        }
                    });
                    iv1.setOnMouseDragged(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent t) {
                            double offsetX = t.getSceneX() - orgSceneX;
                            double offsetY = t.getSceneY() - orgSceneY;
                            double newTranslateX = orgTranslateX + offsetX;
                            double newTranslateY = orgTranslateY + offsetY;
             
                            ((ImageView)(t.getSource())).setTranslateX(newTranslateX);
                            ((ImageView)(t.getSource())).setTranslateY(newTranslateY);
                        }
                    });
                    iv1.setOnMouseReleased(new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent event) {
                        dataManager.setImage1Pos((int) iv1.getTranslateX(), (int) iv1.getTranslateY());
                    }
                        
                    });
                    
                    
    }
    
    public void addImage2(ImageView i2){
        iv2 = i2;
        imagepane.getChildren().add(iv2);
        iv2.setOnMousePressed(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent t) {
                            if(selectedimage == 2){
                                getRemoveImageButton().setDisable(true);
                                iv2.setEffect(null);
                                selectedimage = 0;
                            }
                            else{
                                selectedimage = 2;
                                getRemoveImageButton().setDisable(false);
                                iv2.setEffect(new DropShadow(50, Color.BLUE));
                                orgSceneX = t.getSceneX();
                                orgSceneY = t.getSceneY();
                                orgTranslateX = ((ImageView)(t.getSource())).getTranslateX();
                                orgTranslateY = ((ImageView)(t.getSource())).getTranslateY(); 
                            }
                            
                        }
                        
                    });
                    iv2.setOnMouseDragged(new EventHandler<MouseEvent>(){
                        @Override
                        public void handle(MouseEvent t) {
                            double offsetX = t.getSceneX() - orgSceneX;
                            double offsetY = t.getSceneY() - orgSceneY;
                            double newTranslateX = orgTranslateX + offsetX;
                            double newTranslateY = orgTranslateY + offsetY;
             
                            ((ImageView)(t.getSource())).setTranslateX(newTranslateX);
                            ((ImageView)(t.getSource())).setTranslateY(newTranslateY);
                        }
                    });
                    iv2.setOnMouseReleased(new EventHandler<MouseEvent>(){
                    @Override
                    public void handle(MouseEvent event) {
                        dataManager.setImage2Pos((int) iv2.getTranslateX(), (int) iv2.getTranslateY());
                    }
                        
                    });
    }
    
    public void removeImage(){
        
         if(selectedimage == 1){
            imagepane.getChildren().remove(iv1);
            dataManager.setImage1("", 0, 0);
        }
        else if(selectedimage == 2){
            imagepane.getChildren().remove(iv2);
            dataManager.setImage2("", 0, 0);
        }
    }
    
    
    private void setupHandlers() {
        
        controller = new MapEditorController(app);
        
        
        mapName.setOnAction(e->{
            controller.changeMapName();
        });
        addImage.setOnAction(e->{
            controller.addImage();
        });
        removeImage.setOnAction(e->{
            removeImage();
        });
        mapColor.setOnAction(e->{
            controller.reassignMapColor();
        });
        
        dimension.setOnAction(e->{
            controller.changeMapDimension();
        });
        
        anthem.setOnAction(e->{
            anthem.setDisable(true);
            pause.setDisable(false);
            
            //play anthem?
            String anthempath = dataManager.getMapFolder() + "/" + dataManager.getAnthemPath();
            Sequence sequence;
            if(!(new File(anthempath).exists())){
                pause.setDisable(true);
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Play National Anthem", "This region does not have anthem in its folder.");
            }
            else{
                try {
                sequence = MidiSystem.getSequence(new File(anthempath));
                sequencer = MidiSystem.getSequencer();
                sequencer.open();
                sequencer.setSequence(sequence);
                if (sequencer != null)
                {
                sequencer.setTickPosition(0);
                sequencer.start();
                } 
            } catch (InvalidMidiDataException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MidiUnavailableException ex) {
                Logger.getLogger(Workspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            }
            
        });
        pause.setOnAction(e->{
            pause.setDisable(true);
            anthem.setDisable(false);
            sequencer.stop();
        });
        
        borderColor.setOnAction(e->{
            for(double i = 0; i < dataManager.getPolygonList().size(); i++) {
            PolygonData data = dataManager.getPolygonList().get((int)i);
            for(int j = 0; j < data.getPolygons().size(); j++){
                Polygon p = data.getPolygons().get(j);
                p.strokeProperty().bind(borderColor.valueProperty());
            }
            dataManager.setBorderColorFromPicker(borderColor.getValue());
        }
        });

        bgColor.setOnAction(e->{
            imagepane.setBackground(new Background(new BackgroundFill(bgColor.getValue(), null, null)));
            dataManager.setBgColorFromPicker(bgColor.getValue());
        });
        
        itemsTable.setOnMouseClicked(e -> {
            
            if (e.getClickCount() == 1 && flag == true) {
                Subregion s  = itemsTable.getSelectionModel().getSelectedItem();
                for(int i = 0; i < dataManager.getSubregion().size(); i++){
                    if(dataManager.getSubregion().get(i).equals(s)){
                        highlightPolygon(i);
                        break;
                    }
                }
                
            }
            
            if (e.getClickCount() == 2 && flag == true) {
                Subregion s  = itemsTable.getSelectionModel().getSelectedItem();
                
                for(int i = 0; i < dataManager.getSubregion().size(); i++){
                    if(dataManager.getSubregion().get(i).equals(s)){
                        highlightPolygon(i);
                        controller.processEditSubregion(i);
                        break;
                    }
                }
                
            }
            
            if(flag == true){
                if(lastSelectedIndex == itemsTable.getSelectionModel().getSelectedIndex()){
                    itemsTable.getSelectionModel().clearSelection();
                    flag = false;
                }else{
                    lastSelectedIndex = itemsTable.getSelectionModel().getSelectedIndex();
                }
            }else if(flag == false) {
                lastSelectedIndex = itemsTable.getSelectionModel().getSelectedIndex();
                if (itemsTable.getSelectionModel().getSelectedIndex() !=-1)
                    flag = true;
            }

        });    
        
        app.getGUI().getAppPane().setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent e) {
                switch (e.getCode().toString()) {
                    case "UP":
                        root.requestFocus();
                        root.setTranslateY(root.getTranslateY() -50);
                        dataManager.setmapPos((int)(root.getTranslateX()),(int) (root.getTranslateY()));
                        break;
                    case "RIGHT":
                        root.requestFocus();
                        root.setTranslateX(root.getTranslateX() + 50);
                        dataManager.setmapPos((int)(root.getTranslateX()),(int) (root.getTranslateY()));
                        break;
                    case "DOWN":
                        root.requestFocus();
                        root.setTranslateY(root.getTranslateY() + 50);
                        dataManager.setmapPos((int)(root.getTranslateX()),(int) (root.getTranslateY()));
                        break;
                    case "LEFT":
                        root.requestFocus();
                        root.setTranslateX(root.getTranslateX() - 50);
                        dataManager.setmapPos((int)(root.getTranslateX()),(int) (root.getTranslateY()));
                        break;
                    case "G":
                        break;
                }
                e.consume();
            }
        });
    }
  
    @Override
    public void initStyle() {
        
    }
    
    public void ChangeDimension(int x, int y){
        dataManager.setMapDimension(x, y);
        r1.setWidth(x);
        r1.setHeight(y);
    }
    
    public Rectangle getR1(){
        return r1;
    }
    
    public Button getRemoveImageButton(){
        return removeImage;
    }
    
    public void export(String path){
//        r2.setFill(Color.TRANSPARENT);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        SnapshotParameters sp = new SnapshotParameters();
        sp.setTransform(Transform.scale(1, 1));
        WritableImage image = imagepane.snapshot(sp, null);

        // TODO: probably use a file chooser here
        File file = new File(path + "/" + dataManager.getMapName() + ".png");

        try {
             ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
             dialog.show("Export Map", "Map has Exported.");
        } catch (IOException e) {
            dialog.show("Export Map", "An error occur.");
        }
    }
    
    public void highlightTableRow(int index){
        itemsTable.getSelectionModel().select(index);
    }
    
    public void highlightPolygon(int index){
        
        PolygonData data = dataManager.getPolygonList().get(index);
        if(previous < 0){
            previous = index;
            for(int i = 0; i < data.getPolygons().size(); i++){
                data.getPolygons().get(i).setFill(Color.RED);
            }
        }
        else if(previous == index) {
            PolygonData reset = dataManager.getPolygonList().get(previous);
            Color c = Color.WHITE;
            if(dataManager.getSubregion().get(0) != null){
                c = dataManager.getSubregion().get(previous).getColor();
            }
            for(int i = 0; i < reset.getPolygons().size(); i++){
                reset.getPolygons().get(i).setFill(c);
            }
            previous = 999;
        }
        else{
            if(previous >= 0 && previous < dataManager.getPolygonList().size()){
                PolygonData reset = dataManager.getPolygonList().get(previous);
                Color c = Color.WHITE;
                if(dataManager.getSubregion().get(0) != null){
                    c = dataManager.getSubregion().get(previous).getColor();
                }
                for(int i = 0; i < reset.getPolygons().size(); i++){
                    reset.getPolygons().get(i).setFill(c);
                }
            }
            previous = index;
            for(int i = 0; i < data.getPolygons().size(); i++){
                data.getPolygons().get(i).setFill(Color.RED);
            }
                            
        }
    }

}
