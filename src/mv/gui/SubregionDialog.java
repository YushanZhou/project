/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import mv.data.Subregion;
import properties_manager.PropertiesManager;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import saf.ui.AppMessageDialogSingleton;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;
import static javafx.application.Application.launch;

/**
 *
 * @author yushanzhou
 * 
 */
public class SubregionDialog extends Stage{
    
    
    static SubregionDialog singleton = null;
    //private AppItemDialog itemDialog = null;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane messagePane;
    Scene messageScene;
    Button okButton;
    Button cancelButton;
    Button previous;
    Button next;
    String selection;
    TextField subregionName;
    TextField capitalName;
    TextField leaderName;
    
    Label subregion;
    Label capital;
    Label leader;
    ImageView leaderimage;
    ImageView flagimage;
    
    ObservableList<Subregion> subregionlist;
    int index;
    Subregion s;
    Workspace workspace;
    String mapFolder;
    
    // CONSTANT CHOICES

    /**
     *
     */
    public static final String OK = "Ok";

    /**
     *
     */
    public static final String CANCEL = "Cancel";
    
    
    /**
     * private constructor
     */
    private SubregionDialog() {}
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static SubregionDialog getSingleton() {
        if(singleton == null)
            singleton = new SubregionDialog();
	return singleton;
    }
    
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
              
        // SAVE AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);
        previous = new Button("Previous Subregion");
        next = new Button("Next Subregion");
        

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonbox = new HBox();
        buttonbox.getChildren().add(okButton);
        buttonbox.getChildren().add(cancelButton);
         
        subregionName = new TextField();
        capitalName = new TextField();
        leaderName = new TextField();
        
        subregion = new Label("Subregion Name");
        capital = new Label("Capital Name");
        leader = new Label("Leader Name");
        
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new GridPane();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.setHgap(10);
        messagePane.setVgap(10);
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        
        leaderimage = new ImageView();
        flagimage = new ImageView();
        
        
        messagePane.add(subregion, 0, 1);
        messagePane.add(subregionName, 1, 1);
        messagePane.add(capital, 0, 2);
        messagePane.add(capitalName, 1, 2);
        messagePane.add(leader, 0, 3);
        messagePane.add(leaderName, 1, 3);
        messagePane.add(previous, 0, 4);
        messagePane.add(next, 1, 4);
        messagePane.add(buttonbox, 1, 5);
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
        
        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler OkHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            saveSubregion();
            SubregionDialog.this.hide();
        };
        
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler CancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            
            SubregionDialog.this.hide();
        };
        
        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler PreviousHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            saveSubregion();
            if(index > 0){
                index--;
                messagePane.getChildren().remove(leaderimage);
                messagePane.getChildren().remove(flagimage);
                workspace.highlightPolygon(index);
                workspace.reloadTable();
                workspace.highlightTableRow(index);
                
                s = subregionlist.get(index);
                
                showSubregion();
            }
            else{
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Subregion Message", "This is the first subregion, no previous.");
            }
        };
        
        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler NextHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            saveSubregion();
            if(index >= 0 && index < subregionlist.size() - 1){
                index++;
                messagePane.getChildren().remove(leaderimage);
                messagePane.getChildren().remove(flagimage);
                workspace.highlightPolygon(index);
                workspace.reloadTable();
                workspace.highlightTableRow(index);
                
                s = subregionlist.get(index);
                showSubregion();
            }
            else{
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Subregion Message", "This is the last subregion, no next.");
            }
        };
        
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(OkHandler);
        cancelButton.setOnAction(CancelHandler);
        previous.setOnAction(PreviousHandler);
        next.setOnAction(NextHandler);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public void setWorkspace(Workspace w){
        workspace = w;
    }
    
    public void setMapFolder(String s){
        mapFolder = s;
    }
    
    public void setSubregion(ObservableList<Subregion> sub, int i){
        subregionlist = sub;
        index = i;
        s = subregionlist.get(index);
        showSubregion();
    }
    
    private void showSubregion(){
        subregionName.setText(s.getSubregionName());
        capitalName.setText(s.getCapital());
        leaderName.setText(s.getLeader());
        String flagpath = mapFolder + "/" + s.getSubregionName() + " Flag.png";
        String leaderpath = mapFolder + "/" + s.getLeader() + ".png";
        setLeaderImage(leaderpath);
        setFlagImage(flagpath);
    }
    
    private void saveSubregion(){
        s.setSubregionName(getSubregionName());
        s.setCapital(getcapitalName());
        s.setLeader(getleaderName());
    }
    
    public String getSubregionName(){
        return subregionName.getText();
    }
    
    public String getcapitalName(){
        return capitalName.getText();
    }
    
    public String getleaderName(){
        return leaderName.getText();
    }
    
    private void setLeaderImage(String path){
       if(new File(path).exists()){
           File f = new File(path);
           Image l;
            try {
                l = new Image(f.toURI().toURL().toString());
                 leaderimage = new ImageView(l);
                messagePane.add(leaderimage, 0, 0);
            } catch (MalformedURLException ex) {
                Logger.getLogger(SubregionDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
           
       } 
       else{
           leaderimage = new ImageView();
            messagePane.add(leaderimage, 0, 0);
       }
    }
    
    private void setFlagImage(String path){
        if(new File(path).exists()){
           File f = new File(path);
           Image l;
            try {
                l = new Image(f.toURI().toURL().toString());
                 flagimage = new ImageView(l);
                messagePane.add(flagimage, 1, 0);
            } catch (MalformedURLException ex) {
                Logger.getLogger(SubregionDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
       }
        else{
            flagimage = new ImageView();
             messagePane.add(flagimage, 1, 0);
        }
    }
    
    
    
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }
    
}
