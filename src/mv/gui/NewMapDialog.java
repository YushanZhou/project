/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.gui;

import java.io.File;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import static mv.PropertyType.*;
import properties_manager.PropertiesManager;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import static saf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import static saf.settings.AppStartupConstants.PATH_WORK;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author yushanzhou
 * 
 */
public class NewMapDialog extends Stage{
    
    
    static NewMapDialog singleton = null;
    //private AppItemDialog itemDialog = null;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane messagePane;
    Scene messageScene;
    Button okButton;
    Button cancelButton;
    String selection;
    TextField mapName;
    TextField parentDirectory;
    TextField dataFile;
    Button parentdir;
    Button file;
    
    Label map;
    Label directory;
    Label data;
    
    String polygonpath;
    
    AppMessageDialogSingleton dialog;
    
    
    // CONSTANT CHOICES

    /**
     *
     */
    public static final String OK = "Ok";

    /**
     *
     */
    public static final String CANCEL = "Cancel";
    
    
    /**
     * private constructor
     */
    private NewMapDialog() {}
    
    /**
     * The static accessor method for this singleton.
     * 
     * @return The singleton object for this type.
     */
    public static NewMapDialog getSingleton() {
        if(singleton == null)
            singleton = new NewMapDialog();
	return singleton;
    }
    
    /**
     * This method initializes the singleton for use.
     * 
     * @param primaryStage The window above which this
     * dialog will be centered.
     */
    public void init(Stage primaryStage) {
        
        dialog = AppMessageDialogSingleton.getSingleton();
        
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
              
        // SAVE AND CANCEL BUTTONS
        okButton = new Button(OK);
        cancelButton = new Button(CANCEL);
        

        // NOW ORGANIZE OUR BUTTONS
        HBox buttonBox = new HBox();
        buttonBox.getChildren().add(okButton);
        buttonBox.getChildren().add(cancelButton);
         
        mapName = new TextField();
        parentDirectory = new TextField();
        dataFile = new TextField();
        parentdir = new Button();
        file = new Button();
        String imagePath = "file:./images/Load.png";
        Image buttonImage = new Image(imagePath);
        parentdir.setGraphic(new ImageView(buttonImage));
        file.setGraphic(new ImageView(buttonImage));
        parentdir.setTooltip(new Tooltip("Select parent directory"));
        file.setTooltip(new Tooltip("Select data file"));
        
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        EventHandler parentdirHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            if(mapName.getText().equals("")){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show("Map Name", "please create the map name.");
            }
            else{
                DirectoryChooser dc = new DirectoryChooser();
                dc.setInitialDirectory(new File(PATH_WORK));
                dc.setTitle(props.getProperty(LOAD_WORK_TITLE));
                File selectedFile = dc.showDialog(null);
                String path = selectedFile.getAbsolutePath();
                if(new File(path + "/"+mapName.getText()).mkdir())
                    parentDirectory.setText(path + "/" + mapName.getText());
            }
        };
        
        EventHandler datafileHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
       
            FileChooser fc = new FileChooser();
            fc.setInitialDirectory(new File(PATH_WORK));
            fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
            File selectedFile = fc.showOpenDialog(null);
            
            // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            polygonpath = selectedFile.getAbsolutePath();
            dataFile.setText(polygonpath);
        }

        };
        
        parentdir.setOnAction(parentdirHandler);
        file.setOnAction(datafileHandler);
        
	
        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        
        
        
        
        map = new Label("Map Name");
        directory = new Label("Parent Directory");
        data = new Label("Data File");
        
        
        // WE'LL PUT EVERYTHING HERE
        messagePane = new GridPane();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.setHgap(10);
        messagePane.setVgap(10);
        messagePane.setPadding(new Insets(80, 60, 80, 60));
        
        messagePane.add(map, 0, 0);
        messagePane.add(mapName, 1, 0);
        messagePane.add(directory, 0, 1);
        messagePane.add(parentDirectory, 1, 1);
        messagePane.add(parentdir, 2, 1);
        messagePane.add(data, 0, 2);
        messagePane.add(dataFile, 1, 2);
        messagePane.add(file, 2, 2);
        messagePane.add(buttonBox, 1, 3);
        
        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        this.setScene(messageScene);
        
        
	// MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler NewMapHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            NewMapDialog.this.selection = sourceButton.getText();
            NewMapDialog.this.hide();
        };
        
	// AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        okButton.setOnAction(NewMapHandler);
        cancelButton.setOnAction(NewMapHandler);
    }
    
    public String getPolygonPath(){
        return polygonpath;
    }
    
    
    public String getMapName(){
        return mapName.getText();
    }
    
    public String getMapFolder(){
        return parentDirectory.getText();
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     * 
     * @param title The title to appear in the dialog window bar.
     * 
     * @param message Message to appear inside the dialog.
     */
    public void show(String title) {
	// SET THE DIALOG TITLE BAR TITLE
	setTitle(title);
        
	// AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
	// WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
	// DO MORE WORK.
        showAndWait();
    }

}
