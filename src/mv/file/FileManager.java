/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Platform;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.shape.Polygon;
import javafx.stage.Stage;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import mv.data.DataManager;
import mv.data.PolygonData;
import mv.data.Subregion;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 *
 * @author McKillaGorilla
 */
public class FileManager implements AppFileComponent {
    
    //map data;
    static final String JSON_MAP_NAME = "map_name";
    static final String JSON_POLYGON = "polygon_filepath";
    static final String JSON_MAP_FOLDER = "map_folder_path";
    static final String JSON_MAP_WIDTH = "map_width";
    static final String JSON_MAP_HEIGHT = "map_height";
    static final String JSON_MAP_POSX = "map_pos_x";
    static final String JSON_MAP_POSY = "map_pos_y";
    static final String JSON_MAP_SCALE = "zoom_scale";
    static final String JSON_MAP_ANTHEM = "map_anthem_path";
    static final String JSON_BG_RED = "background_red";
    static final String JSON_BG_GREEN = "background_green";
    static final String JSON_BG_BLUE = "background_blue";
    static final String JSON_BORDER_RED = "border_red";
    static final String JSON_BORDER_GREEN = "border_green";
    static final String JSON_BORDER_BLUE = "border_blue";
    static final String JSON_BORDER_WIDTH = "border_width";
    static final String JSON_IMAGE1 = "image1";
    static final String JSON_IMAGE1_POSX = "image1_pos_x";
    static final String JSON_IMAGE1_POSY = "image1_pos_y";
    static final String JSON_IMAGE2 = "image2";
    static final String JSON_IMAGE2_POSX = "image2_pos_x";
    static final String JSON_IMAGE2_POSY = "image2_pos_y";
    static final String JSON_SUBREGION = "subregions";
    
    //subregion data;
    static final String JSON_HAVE_CAPITAL = "subregions_have_capitals";
    static final String JSON_HAVE_FLAG = "subregions_have_flags";
    static final String JSON_HAVE_LEADER = "subregions_have_leaders";
    static final String JSON_SUBREGION_NAME = "name";
    static final String JSON_CAPITAL = "capital";
    static final String JSON_LEADER = "leader";
    static final String JSON_RED = "red";
    static final String JSON_GREEN = "green";
    static final String JSON_BLUE = "blue";
    static final String JSON_LEADER_PHOTO_PATH = "leader_photo_filepath";
    static final String JSON_FLAG_PATH = "flag_photo_filepath";

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
        
        
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        String name = json.getString(JSON_MAP_NAME);
        dataManager.setMapName(name);
        String polygonPath = json.getString(JSON_POLYGON);
        dataManager.setMapPath(polygonPath);
        String mapFolder = json.getString(JSON_MAP_FOLDER);
        dataManager.setMapFolder(mapFolder);
        
        
        int mapX = getDataAsInt(json, JSON_MAP_WIDTH);
        int mapY = getDataAsInt(json, JSON_MAP_HEIGHT);
        dataManager.setMapDimension(mapX, mapY);
        
        int mapposX = getDataAsInt(json, JSON_MAP_POSX);
        int mapposY = getDataAsInt(json, JSON_MAP_POSY);
        dataManager.setmapPos(mapposX, mapposY);
        
        int zoom = getDataAsInt(json, JSON_MAP_SCALE);
        dataManager.setMapSacle(zoom);
        
        String anthempath = json.getString(JSON_MAP_ANTHEM);
        dataManager.setAnthemPath(anthempath);
        
        int red = getDataAsInt(json, JSON_BG_RED);
        int green = getDataAsInt(json, JSON_BG_GREEN);
        int blue = getDataAsInt(json, JSON_BG_BLUE);
        
        dataManager.setBgColor(red, green, blue);
//        
        red = getDataAsInt(json, JSON_BORDER_RED);
        green = getDataAsInt(json,JSON_BORDER_GREEN);
        blue = getDataAsInt(json, JSON_BORDER_BLUE);
        double width = getDataAsDouble(json, JSON_BORDER_WIDTH);
        dataManager.setBorder(red, green, blue, width);
        
        String image1 = json.getString(JSON_IMAGE1);
        int image1x = getDataAsInt(json, JSON_IMAGE1_POSX);
        int image1y = getDataAsInt(json, JSON_IMAGE1_POSY);
        dataManager.setImage1(image1, image1x, image1y);

        String image2 = json.getString(JSON_IMAGE2);
        int image2x = getDataAsInt(json, JSON_IMAGE2_POSX);
        int image2y = getDataAsInt(json, JSON_IMAGE2_POSY);
        dataManager.setImage2(image2, image2x, image2y);

        JsonArray subregion = json.getJsonArray(JSON_SUBREGION);
        for (int i = 0; i < subregion.size(); i++) {
            JsonObject jsonItem = subregion.getJsonObject(i);
            Subregion item = loadItem(jsonItem);
	    dataManager.getSubregion().add(item);
        }
        loadPolygon(dataManager, polygonPath);
        
    }
    
    public void loadPolygon(AppDataComponent data, String filePath) throws IOException{
        ProgressBar bar  = new ProgressBar(0);
        //ReentrantLock progressLock = new ReentrantLock();
        
        DataManager dataManager = (DataManager)data;
        JsonObject polygon = loadJSONFile(filePath);
        JsonArray jsonRegionArray = polygon.getJsonArray("SUBREGIONS");
        Stage stage = new Stage();
        Scene scene = new Scene(bar);
        stage.setScene(scene);
        stage.show();
        
        
        
        Task<Void> task = new Task<Void>() {
                    double perc;
                    @Override
                    protected Void call() throws Exception {
                        try {
                            //progressLock.lock();
                            
                            for (int i = 0; i < jsonRegionArray.size(); i++) {
                                 int numPolygons = jsonRegionArray.getJsonObject(i).getInt("NUMBER_OF_SUBREGION_POLYGONS");
                                 JsonArray jsonPolygonArray = jsonRegionArray.getJsonObject(i).getJsonArray("SUBREGION_POLYGONS");
                                 perc = (double)((i + 1)/(double)(jsonRegionArray.size()));
                                 PolygonData polygonList = new PolygonData();
                                 for(int j = 0; j < numPolygons; j++) {
                                      JsonArray jsonPointArray = jsonPolygonArray.getJsonArray(j);

                                     Polygon p = new Polygon();
                                     for(int k = 0; k < jsonPointArray.size(); k++) {
                                        JsonObject jsonPoint = jsonPointArray.getJsonObject(k);
                                        p.getPoints().add(getDataAsDouble(jsonPoint, "X"));
                    
                                        p.getPoints().add((getDataAsDouble(jsonPoint, "Y")));
                                    }
                                     polygonList.add(p);
                                 updateProgress(i, jsonRegionArray.size());
                                 Thread.sleep(100);
                                }
                                dataManager.getPolygonList().add(polygonList);
                           
                        }}
                        finally {
			    // WHAT DO WE NEED TO DO HERE?
                            //progressLock.unlock();
                            
                                }
                        return null;
                    }
                
                };
        bar.progressProperty().bind(task.progressProperty());
        task.setOnSucceeded(e->{
            dataManager.updateWorkspace();
            
            stage.hide();
            if(dataManager.getSubregion().size() == 0){
            int rand = 0;
            ObservableList<Subregion> subregion = FXCollections.observableArrayList();
            for(int i = 0; i < dataManager.getPolygonList().size(); i++){
                rand = (int)(Math.random()*254) + 1;
                Subregion s = new Subregion("", "", "", rand, rand, rand, "", "");
                subregion.add(s);
            }
            dataManager.setSubregion(subregion);
        }
        });
                // THIS GETS THE THREAD ROLLING
                Thread thread = new Thread(task);
                thread.start(); 
                
        
        
    }
    
    public Subregion loadItem(JsonObject jsonItem){
        String name = jsonItem.getString(JSON_SUBREGION_NAME);
        String capital = jsonItem.getString(JSON_CAPITAL);
        String leader = jsonItem.getString(JSON_LEADER);
        int red  = getDataAsInt(jsonItem, JSON_RED);
        int green  = getDataAsInt(jsonItem, JSON_GREEN);
        int blue  = getDataAsInt(jsonItem, JSON_BLUE);
        String leaderphoto = jsonItem.getString(JSON_LEADER_PHOTO_PATH);
        String flag = jsonItem.getString(JSON_FLAG_PATH );
        Subregion s = new Subregion(name, capital, leader, red, green, blue, flag, leaderphoto);
        return s;
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        
        // GET THE DATA
	DataManager dataManager = (DataManager)data;
        
        File save = new File(filePath + "/"+dataManager.getMapName() + ".json");
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	ObservableList<Subregion> items = dataManager.getSubregion();
	for (Subregion item : items) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, item.getSubregionName())
		    .add(JSON_CAPITAL, item.getCapital())
		    .add(JSON_LEADER, item.getLeader())
		    .add(JSON_RED, (int)(item.getColor().getRed()*255))
                    .add(JSON_GREEN,(int)(item.getColor().getGreen()*255))
                    .add(JSON_BLUE, (int)(item.getColor().getBlue()*255))
                    .add(JSON_LEADER_PHOTO_PATH, item.getLeaderPhoto())
                    .add(JSON_FLAG_PATH, item.getFlagPath()).build();
	    arrayBuilder.add(itemJson);
	}
	JsonArray itemsArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_MAP_NAME, dataManager.getMapName())
		.add(JSON_POLYGON, dataManager.getMapPath())
                .add(JSON_MAP_FOLDER, dataManager.getMapFolder())
                .add(JSON_MAP_WIDTH, dataManager.getMapDinmensionX())
                .add(JSON_MAP_HEIGHT, dataManager.getMapDimensionY())
                .add(JSON_MAP_POSX, dataManager.getMapPosX())
                .add(JSON_MAP_POSY, dataManager.getMapPosY())
                .add(JSON_MAP_SCALE, dataManager.getMapScale())
                .add(JSON_MAP_ANTHEM, dataManager.getAnthemPath())
                .add(JSON_BG_RED, (int)(dataManager.getBgColor().getRed()*255))
                .add(JSON_BG_GREEN,(int)(dataManager.getBgColor().getGreen()*255))
                .add(JSON_BG_BLUE, (int)(dataManager.getBgColor().getBlue()*255))
                .add(JSON_BORDER_WIDTH, dataManager.getBorderWidth())
                .add(JSON_BORDER_RED,(int)(dataManager.getBorderColor().getRed()*255))
                .add(JSON_BORDER_GREEN, (int)(dataManager.getBorderColor().getGreen()*255))
                .add(JSON_BORDER_BLUE,(int)(dataManager.getBorderColor().getBlue()*255))
                .add(JSON_IMAGE1,dataManager.getImage1Path())
                .add(JSON_IMAGE1_POSX,dataManager.getImage1PosX())
                .add(JSON_IMAGE1_POSY,dataManager.getImage1PosY())
                .add(JSON_IMAGE2,dataManager.getImage2Path())
                .add(JSON_IMAGE2_POSX,dataManager.getImage2PosX())
                .add(JSON_IMAGE2_POSY,dataManager.getImage2PosY())
		.add(JSON_SUBREGION, itemsArray)
		.build();
	
        
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(save);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(save);
	pw.write(prettyPrinted);
	pw.close();
    
    
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
	DataManager dataManager = (DataManager)data;
        
        boolean capital = true, flag = true, leader = true;
        
        File export = new File(filePath + "/"+dataManager.getMapName() + ".rvm");
        
        String exportpath = export.getPath();
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	ObservableList<Subregion> items = dataManager.getSubregion();
        
        for(int i = 0; i < items.size(); i++){
            if(items.get(i).getCapital().equals(""))
                capital = false;
            
            String flagpath = dataManager.getMapFolder()+"/"+items.get(i).getSubregionName()+" Flag.png";
            if(!(new File(flagpath).exists()))
                flag = false;
            
            String leaderpath = dataManager.getMapFolder()+"/"+items.get(i).getLeader()+".png";
            if(items.get(i).getLeader().equals("") || (!(new File(leaderpath).exists())) )
                leader = false;
        }
        
        if(capital && leader){
	   for (Subregion item : items) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, item.getSubregionName())
		    .add(JSON_CAPITAL, item.getCapital())
		    .add(JSON_LEADER, item.getLeader())
		    .add(JSON_RED, (int)(item.getColor().getRed()*255))
                    .add(JSON_GREEN,(int)(item.getColor().getGreen()*255))
                    .add(JSON_BLUE, (int)(item.getColor().getBlue()*255)).build();
	    arrayBuilder.add(itemJson);
	   }
        }
        
        else if(capital && !leader){
            for (Subregion item : items) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, item.getSubregionName())
		    .add(JSON_CAPITAL, item.getCapital())
		    .add(JSON_RED, (int)(item.getColor().getRed()*255))
                    .add(JSON_GREEN,(int)(item.getColor().getGreen()*255))
                    .add(JSON_BLUE, (int)(item.getColor().getBlue()*255)).build();
	    arrayBuilder.add(itemJson);
            }
        }
        
        else if(!capital && leader){
            for (Subregion item : items) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, item.getSubregionName())
		    .add(JSON_LEADER, item.getLeader())
		    .add(JSON_RED, (int)(item.getColor().getRed()*255))
                    .add(JSON_GREEN,(int)(item.getColor().getGreen()*255))
                    .add(JSON_BLUE, (int)(item.getColor().getBlue()*255)).build();
	    arrayBuilder.add(itemJson);
            }
        }
        else{
            for (Subregion item : items) {	    
	    JsonObject itemJson = Json.createObjectBuilder()
		    .add(JSON_SUBREGION_NAME, item.getSubregionName())
		    .add(JSON_RED, (int)(item.getColor().getRed()*255))
                    .add(JSON_GREEN,(int)(item.getColor().getGreen()*255))
                    .add(JSON_BLUE, (int)(item.getColor().getBlue()*255)).build();
	    arrayBuilder.add(itemJson);
            }
        }
	JsonArray itemsArray = arrayBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SUBREGION_NAME, dataManager.getMapName())
		.add(JSON_HAVE_CAPITAL, capital)
                .add(JSON_HAVE_FLAG, flag)
                .add(JSON_HAVE_LEADER,leader)
		.add(JSON_SUBREGION, itemsArray)
		.build();
	

	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(exportpath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(exportpath);
	pw.write(prettyPrinted);
	pw.close();
        
    
    }

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
