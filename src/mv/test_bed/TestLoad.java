/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.test_bed;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import mv.data.DataManager;
import mv.data.Subregion;
import mv.file.FileManager;

/**
 *
 * @author yushanzhou
 */
public class TestLoad {
    
        
    public static void main(String[] args) {
        
        TestLoad testLoad = new TestLoad();
        DataManager dm = testLoad.loadAndorraMap();
        testLoad.loadSanMarinoMap();
        testLoad.loadSlovakiaMap();
        
    }

    public static DataManager loadAndorraMap() {
        FileManager fm = new FileManager();
        DataManager map = new DataManager();
        
        try {
            fm.loadData(map, "./work/Andorra.json");
            System.out.println("map name: " + map.getMapName());
            System.out.println("polygon file path" + map.getMapPath());
            System.out.println("map background color red: " +(int) (map.getBgColor().getRed()*255));
            System.out.println("green: " +(int)( map.getBgColor().getGreen()*255));
            System.out.println("blue; " + (int)(map.getBgColor().getBlue()*255));
            System.out.println("map border width: " + (int)(map.getBorderWidth()));
            System.out.println("map border color red: " +(int) (map.getBorderColor().getRed()*255));
            System.out.println("green: " + (int)(map.getBorderColor().getGreen()*255));
            System.out.println("blue: " + (int)(map.getBorderColor().getBlue()*255));
            System.out.println("image1 filepath: " + map.getImage1Path());
            System.out.println("image1 x-positon: " + map.getImage1PosX());
            System.out.println("image1 y-positon: " + map.getImage1PosY());
            System.out.println("image2 filepath: " + map.getImage2Path());
            System.out.println("image2 x-positon: " + map.getImage2PosX());
            System.out.println("image2 y-positon: " + map.getImage2PosY());

            ObservableList<Subregion> data = map.getSubregion();
            for(int i = 0; i < data.size(); i++){
                System.out.println("Subregion name: " + data.get(i).getSubregionName());
                System.out.println("capital: " + data.get(i).getCapital());
                System.out.println("leader: " + data.get(i).getLeader());
                System.out.println("red: " + (int)(data.get(i).getColor().getRed()*255));
                System.out.println("green: " + (int)(data.get(i).getColor().getGreen()*255));
                System.out.println("blue: " + (int)(data.get(i).getColor().getBlue()*255));
                System.out.println("leader's photo filepath: " + data.get(i).getLeaderPhoto());
                System.out.println("flag filepath: " + data.get(i).getFlagPath());
            }
        } catch (IOException ex) {
            Logger.getLogger(TestLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return map;
        
    }
    
    public static DataManager loadSanMarinoMap() {
        FileManager fm = new FileManager();
        DataManager map = new DataManager();
        
        try {
            fm.loadData(map, "./work/San Marino.json");
            System.out.println("map name: " + map.getMapName());
            System.out.println("polygon file path" + map.getMapPath());
             System.out.println("map background color red: " +(int) (map.getBgColor().getRed()*255));
            System.out.println("green: " +(int)( map.getBgColor().getGreen()*255));
            System.out.println("blue; " + (int)(map.getBgColor().getBlue()*255));
            System.out.println("map border width: " + (int)(map.getBorderWidth()));
            System.out.println("map border color red: " +(int) (map.getBorderColor().getRed()*255));
            System.out.println("green: " + (int)(map.getBorderColor().getGreen()*255));
            System.out.println("blue: " + (int)(map.getBorderColor().getBlue()*255));
            System.out.println("image1 filepath: " + map.getImage1Path());
            System.out.println("image1 x-positon: " + map.getImage1PosX());
            System.out.println("image1 y-positon: " + map.getImage1PosY());
            System.out.println("image2 filepath: " + map.getImage2Path());
            System.out.println("image2 x-positon: " + map.getImage2PosX());
            System.out.println("image2 y-positon: " + map.getImage2PosY());
            ObservableList<Subregion> data = map.getSubregion();
            for(int i = 0; i < data.size(); i++){
                System.out.println("Subregion name: " + data.get(i).getSubregionName());
                System.out.println("leader: " + data.get(i).getLeader());
               System.out.println("red: " + (int)(data.get(i).getColor().getRed()*255));
                System.out.println("green: " + (int)(data.get(i).getColor().getGreen()*255));
                System.out.println("blue: " + (int)(data.get(i).getColor().getBlue()*255));
                System.out.println("leader's photo filepath: " + data.get(i).getLeaderPhoto());
                System.out.println("flag filepath: " + data.get(i).getFlagPath());
                
            }
        } catch (IOException ex) {
            Logger.getLogger(TestLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return map;
    }
    
    public static DataManager loadSlovakiaMap() {
        FileManager fm = new FileManager();
        DataManager map = new DataManager();
        
        try {
            fm.loadData(map, "./work/Slovakia.json");
            System.out.println("map name: " + map.getMapName());
            System.out.println("polygon file path" + map.getMapPath());
            System.out.println("map background color red: " +(int) (map.getBgColor().getRed()*255));
            System.out.println("green: " +(int)( map.getBgColor().getGreen()*255));
            System.out.println("blue; " + (int)(map.getBgColor().getBlue()*255));
            System.out.println("map border width: " + (int)(map.getBorderWidth()));
            System.out.println("map border color red: " +(int) (map.getBorderColor().getRed()*255));
            System.out.println("green: " + (int)(map.getBorderColor().getGreen()*255));
            System.out.println("blue: " + (int)(map.getBorderColor().getBlue()*255));
            System.out.println("image1 filepath: " + map.getImage1Path());
            System.out.println("image1 x-positon: " + map.getImage1PosX());
            System.out.println("image1 y-positon: " + map.getImage1PosY());
            System.out.println("image2 filepath: " + map.getImage2Path());
            System.out.println("image2 x-positon: " + map.getImage2PosX());
            System.out.println("image2 y-positon: " + map.getImage2PosY());
            ObservableList<Subregion> data = map.getSubregion();
            for(int i = 0; i < data.size(); i++){
                System.out.println("Subregion name: " + data.get(i).getSubregionName());
                System.out.println("red: " + (int)(data.get(i).getColor().getRed()*255));
                System.out.println("green: " + (int)(data.get(i).getColor().getGreen()*255));
                System.out.println("blue: " + (int)(data.get(i).getColor().getBlue()*255));
                System.out.println("leader's photo filepath: " + data.get(i).getLeaderPhoto());
                System.out.println("flag filepath: " + data.get(i).getFlagPath());
            }
        } catch (IOException ex) {
            Logger.getLogger(TestLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return map;
    
    }
    
}
