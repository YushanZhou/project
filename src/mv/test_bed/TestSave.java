/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.test_bed;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import mv.RegioVincoMapEditor;
import mv.data.DataManager;
import mv.data.Subregion;
import mv.file.FileManager;

/**
 *
 * @author yushanzhou
 */
public class TestSave {
        
        
    public static void main(String[] args) {
        TestSave testSave = new TestSave();
        testSave.createAndorraMap();
        testSave.createSanMarinoMap();
        testSave.createSlovakiaMap();
////        
        
    }
    
    
    public static DataManager createAndorraMap(){
        
        DataManager map = new DataManager();
        
        FileManager fm = new FileManager();
        
        map.setMapName("AndorraMap");
        
        map.setMapPath("./raw_map_data/Andorra.json");
        
        map.setMapFolder("");
        
        map.setMapDimension(0, 0);
        
        map.setMapSacle(100);
        
        map.setAnthemPath("");
        
        map.setBgColor(233, 98, 0);
        
        map.setBorder(3, 3, 3, 0.001);
        
        map.setImage1("", 27, 34);
//        
        map.setImage2("", 540, 370);
        
        ObservableList<Subregion> data =
        FXCollections.observableArrayList(new Subregion("Ordino", "Ordino (town)", "Ventura Espot", 200, 200, 200, 
                "./export/The World/Europe/Andorra/Ordino Flag.png", "./export/The World/Europe/Andorra/Ventura Espot.png" ),
            new Subregion("Canillo", "Canillo (town)", "Enric Casadevall Medrano", 198, 198, 198,
                    "./export/The World/Europe/Andorra/Canillo Flag.png", "./export/The World/Europe/Andorra/Enric Casadevall Medrano.png"),
            new Subregion("Encamp", "Encamp (town)", "Miquel Alís Font", 196, 196, 196, 
                    "./export/The World/Europe/Andorra/Encamp Flag.png", "./export/The World/Europe/Andorra/Miquel Alís Font.png"),
            new Subregion("Escaldes-Engordany", "Escaldes-Engordany (town)", "Montserrat Capdevila Pallarés", 194, 194, 194, 
                    "./export/The World/Europe/Andorra/Escaldes-Engordany Flag.png", "./export/The World/Europe/Andorra/Montserrat Capdevila Pallarés.png"),
            new Subregion("La Massana", "La Massana (town)", "Josep Areny", 192, 192, 192, 
                    "./export/The World/Europe/Andorra/La Massana Flag.png", "./export/The World/Europe/Andorra/Josep Areny.png"),
            new Subregion("Andorra la Vella", "Andorra la Vella (city)", "Maria Rosa Ferrer Obiols", 190, 190, 190, 
                    "./export/The World/Europe/Andorra/Andorra la Vella Flag.png", "./export/The World/Europe/Andorra/Maria Rosa Ferrer Obiols.png"),
            new Subregion("Sant Julia de Loria", "Sant Julia de Loria (town)", "Josep Pintat Forné", 188, 188, 188, 
                    "./export/The World/Europe/Andorra/Sant Julia de Loria Flag.png", "./export/The World/Europe/AndorraJosep Pintat Forné.png")
        
        );
        
        map.setSubregion(data);
        
        try {
            fm.saveData(map, "./work/Andorra.json");
        } catch (IOException ex) {
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
         return map;

    }
    
    public static DataManager createSanMarinoMap(){
        
        DataManager map = new DataManager();
        
        FileManager fm = new FileManager();
        
        map.setMapName("SanMarinoMap");
        
        map.setMapPath("./raw_map_data/San Marino.json");
        
         map.setMapFolder("");
        
        
         map.setMapDimension(0, 0);
        
        map.setMapSacle(100);
        
        map.setAnthemPath("");
        
        map.setBgColor(233, 98, 0);
        
        map.setBorder(3, 3, 3, 0.001);
        
        map.setImage1("", 0, 0);
//        
        map.setImage2("", 0, 0);
        
        ObservableList<Subregion> data =
        FXCollections.observableArrayList(new Subregion("Acquaviva", "", "Lucia Tamagnini", 225, 225, 225, 
                "./export/The World/Europe/San Marino/Acquaviva Flag.png", "./export/The World/Europe/San Marino/Lucia Tamagnini.png" ),
            new Subregion("Borgo Maggiore", "", "Sergio Nanni", 200, 200, 200, 
                "./export/The World/Europe/San Marino/Borgo Maggiore Flag.png", "./export/The World/Europe/San Marino/Sergio Nanni.png" ),
            new Subregion("Chiesanuova", "", "Franco Santi", 175, 175, 175, 
                "./export/The World/Europe/San Marino/Chiesanuova Flag.png", "./export/The World/Europe/San Marino/Franco Santi.png" ),
            new Subregion("Domagnano", "", "Gabriel Guidi", 150, 150, 150, 
                "./export/The World/Europe/San Marino/Domagnano Flag.png", "./export/The World/Europe/San Marino/Gabriel Guidi.png" ),
            new Subregion("Faetano", "", "Pier Mario Bedetti", 125, 125, 125, 
                "./export/The World/Europe/San Marino/Faetano Flag.png", "./export/The World/Europe/San Marino/Pier Mario Bedetti.png" ),
            new Subregion("Fiorentino", "", "Gerri Fabbri", 100, 100, 100, 
                "./export/The World/Europe/San Marino/Fiorentino Flag.png", "./export/The World/Europe/San Marino/Gerri Fabbri.png" ),
            new Subregion("Montegiardino", "", "Marta Fabbri", 75, 75, 75, 
                "./export/The World/Europe/San Marino/Montegiardino Flag.png", "./export/The World/Europe/San Marino/Marta Fabbri.png" ),
            new Subregion("City of San Marino", "", "Maria Teresa Beccari", 50, 50, 50, 
                "./export/The World/Europe/San Marino/City of San Marino Flag.png", "./export/The World/Europe/San Marino/Maria Teresa Beccari.png" ),
            new Subregion("Serravalle", "", "Leandro Maiani", 25, 25, 25, 
                "./export/The World/Europe/San Marino/Serravalle Flag.png", "./export/The World/Europe/San Marino/Leandro Maiani.png" )
              
        );
        
        map.setSubregion(data);
        
        try {
            fm.saveData(map, "./work/San Marino.json");
        } catch (IOException ex) {
            System.out.println("www");
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
         return map;

    }
    
    public static DataManager createSlovakiaMap(){
        
        DataManager map = new DataManager();
        
        FileManager fm = new FileManager();
        
        map.setMapName("SlovakiaMap");
        
        map.setMapPath("./raw_map_data/Slovakia.json");
        
         map.setMapFolder("");
        
        
         map.setMapDimension(0, 0);
        
        map.setMapSacle(100);
        
        map.setAnthemPath("");
        
        map.setBgColor(233, 98, 0);
        
        map.setBorder(3, 3, 3, 0.001);
        
        map.setImage1("", 27, 34);
//        
        map.setImage2("", 540, 370);
        
        ObservableList<Subregion> data =
        FXCollections.observableArrayList(new Subregion("Bratislava", "", "", 250, 250, 250, "", ""),
            new Subregion("Trnava", "", "", 249, 249, 249, "", ""),
            new Subregion("Trencin", "", "", 248, 248, 249, "", ""),
            new Subregion("Nitra", "", "", 247, 247, 247, "", ""),
            new Subregion("Zilina", "", "", 246, 246, 246, "", ""),
            new Subregion("Banska Bystrica", "", "", 245, 245, 245, "", ""),
            new Subregion("Presov", "", "", 244, 244, 244, "", ""),
            new Subregion("Kosice", "", "", 243, 243, 243, "", "")
        
        );
       
        map.setSubregion(data);
        
        try {
            fm.saveData(map, "./work/Slovakia.json");
        } catch (IOException ex) {
            System.out.println("www");
            Logger.getLogger(TestSave.class.getName()).log(Level.SEVERE, null, ex);
        }
        return map;
        
    }
    
    
    
    

}
