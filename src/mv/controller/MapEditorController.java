/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv.controller;

import java.io.File;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import mv.data.DataManager;
import mv.data.PolygonData;
import mv.data.Subregion;
import mv.file.FileManager;
import mv.gui.MapDimensionDialog;
import mv.gui.NewMapDialog;
import mv.gui.SubregionDialog;
import mv.gui.Workspace;
import properties_manager.PropertiesManager;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import static saf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static saf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static saf.settings.AppPropertyType.LOAD_WORK_TITLE;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import static saf.settings.AppStartupConstants.PATH_WORK;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author yushanzhou
 */
public class MapEditorController {
    AppTemplate app;
    DataManager dataManager;
    FileManager fileManager;
    Workspace workspace;
    
    double orgSceneX, orgSceneY;
    double orgTranslateX, orgTranslateY;
    int selectedimage;
    ImageView iv1;
    ImageView iv2;
    
    AppMessageDialogSingleton message;
    
    
    public MapEditorController(AppTemplate initApp) {
	app = initApp;
        workspace = (Workspace)app.getWorkspaceComponent();
        try {
            dataManager = (DataManager)app.getDataComponent();
            fileManager = (FileManager)app.getFileComponent();
        } catch (Exception ex) {
            Logger.getLogger(MapEditorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeMapName() {
        message = AppMessageDialogSingleton.getSingleton();
        String newName = "";
        TextInputDialog dialog =  new TextInputDialog();
        dialog.setTitle("Change Map Name");
        dialog.setHeaderText("Enter new name of this map:");
        dialog.setContentText(dataManager.getMapName());
        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()){
            newName = result.get();
            dataManager.setMapName(newName);
            dataManager.setAnthemPath(newName + " National Anthem.mid");
            File f1 = new File(dataManager.getMapFolder());
            File f2 = new File(f1.getParent() + "/" + newName);
            boolean success = f1.renameTo(f2);
            if(success){
                dataManager.setMapFolder(f2.getAbsolutePath());
                message.show("Change Map Name", "Map name has changed!");
            }
            else{
                message.show("Change Map Name", "Cannot change map name.");
            }
        }
    }

    public void addImage() {
        // WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	workspace = (Workspace)app.getWorkspaceComponent();
        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(PATH_WORK));
	fc.setTitle(props.getProperty(LOAD_WORK_TITLE));
        File selectedFile = fc.showOpenDialog(app.getGUI().getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                if(dataManager.getImage1Path().equals("") ){
                    iv1 = new ImageView(new Image(selectedFile.toURI().toURL().toString()));
                    dataManager.setImage1(selectedFile.toURI().toURL().toString() , 0, 0);
                    workspace.addImage1(iv1);
                }
                else{
                    iv2 = new ImageView(new Image(selectedFile.toURI().toURL().toString()));
                    dataManager.setImage2(selectedFile.toURI().toURL().toString() , 0, 0);
                    workspace.addImage2(iv2);
                    
                }
                
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
            }
        }
    
    }


    public void reassignMapColor() {
     	workspace = (Workspace)app.getWorkspaceComponent();
        int rand = (int)(Math.random() * 254) + 1;
        for(int i = 0; i < dataManager.getPolygonList().size(); i++ ){
            int random = (rand + (15 * i)) %254;
            dataManager.getSubregion().get(i).setColor(random, random, random);
        }
        workspace.reloadMap();
    }

    public void changeMapDimension() {
        workspace = (Workspace)app.getWorkspaceComponent();
        MapDimensionDialog dialog = MapDimensionDialog.getSingleton();
        dialog.setWorkspace(workspace);
        dialog.show("Change Map Dimension");

    }

    public void processEditSubregion(int i) {
        workspace = (Workspace)app.getWorkspaceComponent();
        
        SubregionDialog dialog = SubregionDialog.getSingleton();
        dialog.setMapFolder(dataManager.getMapFolder());
        dialog.setWorkspace(workspace);
        dialog.setSubregion(dataManager.getSubregion(), i);
        dialog.show("Edit Subregion");
        
        workspace.reloadTable();
    }

}
