/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv;

import javafx.application.Application;
import javafx.stage.Stage;
import mv.gui.SubregionDialog;

/**
 *
 * @author yushanzhou
 */
public class SubregionDialogDriver extends Application{
    
    public static void main(String[] args) {
	launch(args);
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
         SubregionDialog d = SubregionDialog.getSingleton();
         d.init(primaryStage);

         d.show("Edit Subregion Data");
    }
    
}
