/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mv;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import mv.gui.NewMapDialog;

/**
 *
 * @author yushanzhou
 */
public class NewMapDriver extends Application{
    
    public static void main(String[] args) {
	launch(args);
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
         NewMapDialog d = NewMapDialog.getSingleton();
         d.init(primaryStage);

         d.show("Create New Map");
    }
    
}
